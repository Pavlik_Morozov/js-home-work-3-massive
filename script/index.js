let styles = ['Джаз','Блюз'];
document.write('Створений масив: ' + styles.join(' &#9672; ') + '.</br>');
document.write('<hr>');

styles.push('Рок-н-рол');
document.write('Додане до кінця значення: ' + styles.join(' &#9672; ') + '.</br>');
document.write('<hr>');

styles.splice((styles.length-1)/2, 1,'Класика');
document.write('Заміна значення в середині: ' + styles.join(' &#9672; ') + '.</br>');
document.write('<hr>');

let del = styles.shift();
document.write('Видалений елемент масиву: ' + del + '.</br>');
document.write('Залишок масиву: ' + styles.join(' &#9672; ') + '.</br>');
document.write('<hr>');

styles.unshift('Реп','Реггі');
document.write(`Додані у початок значення: ${styles.join(' &#9672; ')} .</br>`);
document.write('<hr>');

let a = ['a', 'b', 'c'];
let b = [1, 2, 3];
